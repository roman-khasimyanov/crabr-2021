import {
  List,
  Row,
  Col,
  useSelect,
  useMany,
  useOne,
  TagField,
  useList,
  GetListResponse,
} from "@pankod/refine";
import { Button, Select, Steps, Typography } from "antd";
import { definitions } from "interfaces/supabase";
import { useEffect, useState } from "react";
import { CloseOutlined } from '@ant-design/icons';

import './style.scss';
import { WinnerCard } from "./winner-card";

const DEFAULT_DEPS = [
  'РЦР Москва',
  'РЦР Санкт-Петербург',
  'РЦР Екатеринбург',
  'РЦР Казань',
  'РЦТиКК Санкт-Петербург',
];

export const NextOrganizationGenerator: React.FC = () => {
  const { data: allData } = useList<definitions['Department']>({
    resource: 'Department',
    config: {
      pagination: { pageSize: 10000 },
    }
  });

  const { data: defaultList, refetch: defaultListRefetch } = useList<definitions['Department']>({
    resource: 'Department',
    config: {
      filters: [{ field: 'name', operator: 'in', value: DEFAULT_DEPS }]
    }
  });

  const [defaultValue, setDefaultValue] = useState<string[]>(defaultList?.data.map((dd) => dd.id) ?? []);

  useEffect(() => {
    setSelectedValues(defaultList?.data.map((dd) => dd.id) ?? []);
  }, [defaultList]);

  const [selectedValues, setSelectedValues] = useState<string[]>([]);
  const [currentStep, setCurrentStep] = useState<number>(0);
  const [foundDepartmentId, setFoundDepartmentId] = useState<string>();

  const findRandomDepartment = () => {
    if (selectedValues.length) {
      const winnerIndex = Math.floor(Math.random() * selectedValues.length);
      setFoundDepartmentId(selectedValues[winnerIndex]);
      setCurrentStep(2);
    }
  }

  useEffect(() => {
    if (selectedValues.length) {
      setCurrentStep(1);
    } else {
      setCurrentStep(0);
    }
  }, [selectedValues]);

  return (
    <List
      pageHeaderProps={{
        title: 'Выбор организатора'
      }}
    >
      <Steps
        type="navigation"
        current={currentStep}
        percent={100}
      >
        <Steps.Step title="Отбор участвующих департаментов" />
        <Steps.Step title="Случайный выбор организатора" />
        <Steps.Step title="Победитель" />
      </Steps>
      <Row>
        <Col span={8} style={{ display: 'flex', marginTop: '15px' }}>
          <Select
            value={selectedValues}
            placeholder="Выбрать департаменты"
            onChange={(values) => setSelectedValues(values as unknown as string[])}
            mode="multiple"
            allowClear
            showSearch={false}
            style={{ width: '70%', margin: 'auto' }}
          >
            {allData?.data.map((d) => (<Select.Option value={d.id}>{d.name}</Select.Option>))}
          </Select>
        </Col>
        <Col span={8} style={{ display: 'flex', marginTop: '15px' }}>
          <Button
            disabled={!selectedValues.length || currentStep === 2}
            style={{ margin: 'auto' }}
            type="primary"
            onClick={() => {
              findRandomDepartment();
            }}
          >
            Выбор организатора
          </Button>
        </Col>
        <Col span={8}>
          {foundDepartmentId && currentStep === 2 && <WinnerCard departmentId={foundDepartmentId} />}
        </Col>
      </Row>
    </List>
  );
};