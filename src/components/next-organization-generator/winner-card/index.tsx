import { useOne } from "@pankod/refine";
import { Card, Progress } from "antd";
import { definitions } from "interfaces/supabase";
import { MutableRefObject, useEffect, useRef, useState } from "react";

export type WinnerCardProps = {
  departmentId: string;
}

export const WinnerCard = ({ departmentId }: WinnerCardProps): JSX.Element => {
  const { data } = useOne<definitions['Department']>({ resource: 'Department', id: departmentId });
  const [currentPercents, setCurrentPercents] = useState<number>(0);
  const ref = useRef<NodeJS.Timeout>(null) as MutableRefObject<NodeJS.Timeout>;

  useEffect(() => {
    ref.current = setTimeout(() => {
      setCurrentPercents(currentPercents + 1);
    }, 20);

    setTimeout(() => {
      clearInterval(ref.current);
    }, 3000);

  }, [data, currentPercents]);

  return (
    <>
      <Progress percent={currentPercents} />
      {currentPercents >= 100 && <div style={{ width: '100%', textAlign: 'center', fontWeight: 600 }}>{data?.data.name}</div>}
    </>
  )
};