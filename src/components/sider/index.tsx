import React from "react";
import { Link, Menu, useMenu, useTitle } from "@pankod/refine";

export const CustomSider: React.FC = () => {
  const Title = useTitle();
  const { menuItems, selectedKey } = useMenu();

  return (
    <>
      <Title collapsed={false} />
      <Menu selectedKeys={[selectedKey]} mode="horizontal">
        {menuItems
          .filter(item => !item.route.startsWith('/admin/'))
          .map(({ route, label }) => (
            <Menu.Item key={route}>
              <Link to={route}>{label}</Link>
            </Menu.Item>
          ))}
      </Menu>
    </>
  );
};
