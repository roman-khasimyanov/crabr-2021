import {
  AntdList,
  List,
  Card,
  Link,
} from "@pankod/refine";

export const DictionariesList: React.FC = () => {
  const dictionaries = [
    {
      title: 'Департаменты',
      link: 'admin/department',
    },
    {
      title: 'Мероприятия',
      link: 'admin/activity',
    },
    {
      title: 'Участники',
      link: 'admin/user',
    },
    {
      title: 'Секции',
      link: 'admin/section',
    },
    {
      title: 'События',
      link: 'admin/event',
    },
  ];

  return (
    <List
      pageHeaderProps={{
        title: 'Справочники'
      }}
    >
      <AntdList<{ title: string, link: string, }>
        grid={{ gutter: 16, column: 4 }}
        dataSource={dictionaries}
        renderItem={item => (
          <AntdList.Item>
            <Link to={item.link}>
              <Card hoverable>{item.title}</Card>
            </Link>
          </AntdList.Item>
        )}
      />
    </List>
  );
};