import { TagField, useList, useMany, useOne } from "@pankod/refine";
import { Button, Card, Image } from "antd";
import { definitions } from "interfaces/supabase";
import moment from "moment";
import { MutableRefObject, useEffect, useRef, useState } from "react";
import "./style.scss";

type SheduleState = Array<{
  dayName: string;
  sections: Array<{
    sectionName: string;
    sectionDescription: string;
    sectionIcon: string;
    events: Array<{
      eventName: string;
      speakers: Array<definitions['User']>;
      startTime: string;
      endTime: string;
    }>
  }>
}>;

function onlyUnique(value: string, index: number, self: Array<string>) {
  return self.indexOf(value) === index;
}

export const Shedule = (): JSX.Element => {
  const { data: events } = useList<definitions['Event']>({ resource: 'Event', config: { sort: [{ field: 'start_date', order: 'asc' }], pagination: { pageSize: 10000 } } });
  const { data: sections, refetch: sectionsRefetch } = useList<definitions['Section']>({ resource: 'Section', config: { pagination: { pageSize: 10000 } } })

  const { data: speakerData } = useList<definitions['EventUser']>({
    resource: 'EventUser',
    config: { pagination: { pageSize: 10000 } }
  });

  const { data: userData } = useList<definitions['User']>({
    resource: 'User',
    config: { pagination: { pageSize: 10000 } }
  });

  const { data: departments } = useList<definitions['Department']>({
    resource: 'Department',
    config: { pagination: { pageSize: 10000 } }
  });

  const [sheduleState, setSheduleState] = useState<SheduleState>([]);
  const [selectedDayIndex, setSelectedDayIndex] = useState<number>(0);

  useEffect(() => {
    sectionsRefetch();
  }, [events]);


  useEffect(
    () => {
      const sortedEvents = events?.data;

      const days = sortedEvents
        ?.map(event => moment(event.start_date).format("DD.MM"))
        .filter(onlyUnique);

      const result: any = days?.map(day => ({
        dayName: day,
        sections: sortedEvents
          ?.filter(event => moment(event.start_date).format("DD.MM") === day)
          .map(event => event.section_id as string)
          .filter(onlyUnique)
          .map((section_id) => sections?.data.find((section) => section.id === section_id))
          .map((section) => ({
            sectionName: section?.name,
            sectionDescription: section?.description,
            sectionIcon: section?.image,
            events: sortedEvents
              .filter(event => event.section_id === section?.id)
              .map(event => {
                const filteredSpeakers = speakerData?.data
                  .filter((s) => s.event_id === event.id)
                  .map((s) => s.user_id);

                const filteredUsers = userData?.data.filter((u) => filteredSpeakers?.includes(u.id))

                return {
                  eventName: event.name,
                  startTime: moment(event.start_date).format('HH:mm'),
                  endTime: moment(event.end_date).format('HH:mm'),
                  speakers: filteredUsers,
                }
              })
          }))
      }))

      setSheduleState(result);
      console.log(result);
    },
    [sections, events, speakerData, userData],
  );

  return (
    <div className="shedule">
      <div className="shedule__buttons" >
        {sheduleState && sheduleState.map((day, index) => <Button onClick={() => setSelectedDayIndex(index)} key={index} type='default'>{day.dayName}</Button>)}
      </div>
      {sheduleState && (
        <>
          {sheduleState[selectedDayIndex]?.sections?.map((section, index) => (
            <>
              {section.sectionName && (
                <Card key={index}>
                  <Card.Meta title={section.sectionName} description={(
                    <div style={{ display: 'flex', alignItems: 'flex-start', marginBottom: '5px' }}>
                      {section.sectionIcon && <Image width={70} src={`https://ekctgodsrwugzvvfebke.supabase.in/storage/v1/object/public/${section.sectionIcon}`} />}
                      <div style={{ marginLeft: '5px' }}>
                        {section.sectionDescription}
                      </div>
                    </div>
                  )} />
                  {section?.events?.map((event, index) => (
                    <Card key={index} >
                      <Card.Meta title={event.eventName} description={
                        <div>
                          <div>
                            {`${event.startTime}-${event.endTime}`}
                          </div>
                          {event
                            ?.speakers
                            ?.map((fu) => <TagField key={fu.id} value={`${fu.last_name} ${fu.name} ${fu.second_name} (${departments?.data.find((d) => d.id === fu.department_id)?.name ?? 'Департамент не выбран'})`} />)
                          }
                        </div>
                      } />
                    </Card>
                  ))}
                </Card>
              )}
            </>
          ))}
        </>
      )}
    </div>
  );
};