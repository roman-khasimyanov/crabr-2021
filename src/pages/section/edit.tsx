import { useForm, Form, Input, Select, Edit, useSelect } from "@pankod/refine";
import { definitions } from "interfaces/supabase";
import { Button, DatePicker, Switch, Upload } from 'antd';
import moment from 'moment';
import { useEffect } from "react";
import { GlobalOutlined, UploadOutlined } from '@ant-design/icons';
import { supabaseClient } from "App";

export const SectionEdit: React.FC = () => {
  const { formProps, saveButtonProps, queryResult, form } = useForm<definitions['Section']>({
    resource: 'Section',
  });

  const { selectProps } = useSelect<definitions['Activity']>({
    resource: 'Activity',
    optionLabel: 'name',
  })

  useEffect(() => {
    if (queryResult?.data?.data) {
      form.setFieldsValue({
        ...queryResult?.data?.data,
      });
    }
  }, [queryResult]);

  return (
    <Edit saveButtonProps={saveButtonProps} title="Редактирование мероприятия">
      <Form
        {...formProps}
        layout="vertical"
      >
        <Form.Item label="Название" name="name">
          <Input />
        </Form.Item>
        <Form.Item label="Описание" name="description">
          <Input.TextArea />
        </Form.Item>
        <Form.Item label="Ссылка" name="link">
          <Input prefix={<GlobalOutlined />} />
        </Form.Item>
        <Form.Item label="Картинка" name="image">
          <Upload
            maxCount={1}
            beforeUpload={(file) => {
              supabaseClient
                .storage
                .from('bucket')
                .upload(Date.now() + file.name, file, {
                  cacheControl: '3600',
                  upsert: false
                })
                .then((response) => {
                  form.setFieldsValue({
                    image: response.data?.Key,
                  });
                });
              return false;
            }}
          >
            <Button icon={<UploadOutlined />}>Загрузить файл</Button>
          </Upload>
        </Form.Item>
        <Form.Item label="Мероприятие" name="activity_id">
          <Select {...selectProps} />
        </Form.Item>
      </Form>
    </Edit>
  );
};