import {
  List,
  TextField,
  DateField,
  Table,
  useTable,
  ShowButton,
  EditButton,
  useNavigation,
  DeleteButton,
  TagField,
  useOne,
  useMany,
  Link,
} from "@pankod/refine";
import { Space, Switch, Image } from 'antd';
import { definitions } from "interfaces/supabase";

export const SectionList: React.FC = () => {
  const { tableProps, tableQueryResult } = useTable<definitions['Section']>({
    resource: 'Section',
  });
  const { data: activityData } = useMany<definitions['Activity']>({
    resource: 'Activity',
    ids: tableQueryResult.data?.data.map((val) => val.activity_id ?? '') ?? [],
  })
  const history = useNavigation();
  console.log(history)
  return (
    <List
      pageHeaderProps={{
        title: 'Секции',
        onBack: () => history.goBack(),
      }}
      createButtonProps={{
        children: 'Добавить',
        resource: 'Section',
        onClick: (e) => {
          history.push(`Section/create`);
        }
      }}
    >
      <Table {...tableProps} rowKey="id">
        <Table.Column
          dataIndex="name"
          title="Название"
          render={(value) => <TextField value={value} />}
        />
        <Table.Column
          dataIndex="description"
          title="Описание"
          render={(value) => <TextField value={value} />}
        />
        <Table.Column
          dataIndex="link"
          title="Ссылка для подключения"
          render={(value) => <Link to={value} target="_blank">{value}</Link>}
        />
        <Table.Column
          dataIndex="image"
          title="Картинка"
          render={(value: string) => ((
            value &&
            (
              value.endsWith('.jpg')
              || value.endsWith('.png')
              || value.endsWith('.gif')
            )
          ) ? <Image width={150} src={`https://ekctgodsrwugzvvfebke.supabase.in/storage/v1/object/public/${value}`} />
            : <Link to={`https://ekctgodsrwugzvvfebke.supabase.in/storage/v1/object/public/${value}`}>{value}</Link>
          )}
        />
        <Table.Column
          dataIndex="activity_id"
          title="Мероприятие"
          render={(value) => <TagField value={activityData?.data.find((d) => d.id === value)?.name} />}
        />
        <Table.Column<definitions['Section']>
          title="Действия"
          dataIndex="actions"
          render={(_text, record): React.ReactNode => {
            return (
              <Space>
                <EditButton
                  size="small"
                  recordItemId={record.id}
                  hideText
                />
                <DeleteButton
                  size="small"
                  recordItemId={record.id}
                  hideText
                />
              </Space>
            );
          }}
        />
      </Table>
    </List>
  );
};