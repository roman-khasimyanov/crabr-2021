import {
  Create,
  Form,
  Input,
  Select,
  useForm,
  useMany,
  useSelect
} from "@pankod/refine";
import { definitions } from "interfaces/supabase";
import { Button, DatePicker, Upload } from 'antd';
import { GlobalOutlined, UploadOutlined } from '@ant-design/icons';
import { supabaseClient } from "App";

export const SectionCreate = () => {

  const { formProps, saveButtonProps, form } = useForm<definitions['Section']>({
    resource: 'Section',
  });

  const { selectProps } = useSelect<definitions['Activity']>({
    resource: 'Activity',
    optionLabel: 'name',
  })

  return (
    <Create
      saveButtonProps={saveButtonProps}
      title="Добавить секцию"
    >
      <Form
        {...formProps}
        layout="vertical"
      >
        <Form.Item label="Название" name="name">
          <Input />
        </Form.Item>
        <Form.Item label="Описание" name="description">
          <Input.TextArea />
        </Form.Item>
        <Form.Item label="Ссылка" name="link">
          <Input prefix={<GlobalOutlined />} />
        </Form.Item>
        <Form.Item label="Картинка" name="image">
          <Upload
            maxCount={1}
            beforeUpload={(file) => {
              supabaseClient
                .storage
                .from('bucket')
                .upload(Date.now() + file.name, file, {
                  cacheControl: '3600',
                  upsert: false
                })
                .then((response) => {
                  form.setFieldsValue({
                    image: response.data?.Key,
                  });
                });
              return false;
            }}
          >
            <Button icon={<UploadOutlined />}>Загрузить файл</Button>
          </Upload>
        </Form.Item>
        <Form.Item label="Мероприятие" name="activity_id">
          <Select {...selectProps} />
        </Form.Item>
      </Form>
    </Create>
  );
};