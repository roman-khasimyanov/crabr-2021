import { useForm, Form, Input, Select, Edit, useSelect } from "@pankod/refine";
import { definitions } from "interfaces/supabase";
import { DatePicker, Switch } from 'antd';
import moment from 'moment';
import { useEffect } from "react";

export const UserEdit: React.FC = () => {
  const { formProps, saveButtonProps, queryResult, form } = useForm<definitions['User']>({
    resource: 'User',
  });

  const { selectProps } = useSelect<definitions['Department']>({
    resource: 'Department',
    optionLabel: 'name',
    filters: [{ field: 'hidden', value: false, operator: 'eq' }],
  })

  useEffect(() => {
    if (queryResult?.data?.data) {
      form.setFieldsValue({
        ...queryResult?.data?.data,
      });
    }
  }, [queryResult]);

  return (
    <Edit saveButtonProps={saveButtonProps} title="Редактирование мероприятия">
      <Form
        {...formProps}
        layout="vertical"
      >
        <Form.Item label="Фамилия" name="last_name">
          <Input />
        </Form.Item>
        <Form.Item label="Имя" name="name">
          <Input />
        </Form.Item>
        <Form.Item label="Отчество" name="second_name">
          <Input />
        </Form.Item>
        <Form.Item label="Подразделение" name="department_id">
          <Select {...selectProps} />
        </Form.Item>
      </Form>
    </Edit>
  );
};