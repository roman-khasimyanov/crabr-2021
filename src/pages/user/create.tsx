import {
  Create,
  Form,
  Input,
  Select,
  useForm,
  useMany,
  useSelect
} from "@pankod/refine";
import { definitions } from "interfaces/supabase";
import { DatePicker } from 'antd';

export const UserCreate = () => {

  const { formProps, saveButtonProps, form } = useForm<definitions['User']>({
    resource: 'User',
  });

  const { selectProps } = useSelect<definitions['Department']>({
    resource: 'Department',
    optionLabel: 'name',
    filters: [{ field: 'hidden', value: false, operator: 'eq' }],
  })

  return (
    <Create
      saveButtonProps={saveButtonProps}
      title="Добавить спикера"
    >
      <Form
        {...formProps}
        layout="vertical"
      >
        <Form.Item label="Фамилия" name="last_name">
          <Input />
        </Form.Item>
        <Form.Item label="Имя" name="name">
          <Input />
        </Form.Item>
        <Form.Item label="Отчество" name="second_name">
          <Input />
        </Form.Item>
        <Form.Item label="Подразделение" name="department_id">
          <Select {...selectProps} />
        </Form.Item>
      </Form>
    </Create>
  );
};