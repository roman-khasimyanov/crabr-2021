import {
  List,
  TextField,
  DateField,
  Table,
  useTable,
  ShowButton,
  EditButton,
  useNavigation,
  DeleteButton,
  TagField,
  useOne,
  useMany,
} from "@pankod/refine";
import { Space, Switch } from 'antd';
import { definitions } from "interfaces/supabase";

export const UserList: React.FC = () => {
  const { tableProps, tableQueryResult } = useTable<definitions['User']>({
    resource: 'User',
  });

  const { data: departmentData } = useMany<definitions['Department']>({
    resource: 'Department',
    ids: tableQueryResult.data?.data.map((val) => val.department_id ?? '') ?? [],
  })

  const history = useNavigation();
  console.log(history)
  return (
    <List
      pageHeaderProps={{
        title: 'Спикеры',
        onBack: () => history.goBack(),
      }}
      createButtonProps={{
        children: 'Добавить',
        resource: 'User',
        onClick: (e) => {
          history.push(`User/create`);
        }
      }}
    >
      <Table {...tableProps} rowKey="id">
        <Table.Column
          dataIndex="last_name"
          title="Фамилия"
          render={(value) => <TextField value={value} />}
        />
        <Table.Column
          dataIndex="name"
          title="Имя"
          render={(value) => <TextField value={value} />}
        />
        <Table.Column
          dataIndex="second_name"
          title="Отчество"
          render={(value) => <TextField value={value} />}
        />
        <Table.Column
          dataIndex="department_id"
          title="Подразделение"
          render={(value) => <TextField value={departmentData?.data.find((d) => d.id === value)?.name} />}
        />
        <Table.Column<definitions['User']>
          title="Действия"
          dataIndex="actions"
          render={(_text, record): React.ReactNode => {
            return (
              <Space>
                <EditButton
                  size="small"
                  recordItemId={record.id}
                  hideText
                />
                <DeleteButton
                  size="small"
                  recordItemId={record.id}
                  hideText
                />
              </Space>
            );
          }}
        />
      </Table>
    </List>
  );
};