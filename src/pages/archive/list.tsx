import {
  List,
  TextField,
  DateField,
  Table,
  useTable,
  ShowButton,
  EditButton,
  useNavigation,
  DeleteButton,
  TagField,
  useOne,
  useMany,
  Link,
  useSelect,
  useList,
} from "@pankod/refine";
import { Card, Col, Row, Space, Switch } from 'antd';
import { definitions } from "interfaces/supabase";

const title = [
  'Ваш шедевр готов!',
  'Равным образом рамки!',
  'Место обучения кадров позволяет!',
];

const text = [
  'Товарищи! консультация с широким активом позволяет выполнять важные задания по разработке соответствующий условий активизации. Идейные соображения высшего порядка, а также постоянный количественный рост и сфера нашей активности способствует подготовки и реализации направлений прогрессивного развития. ',
  'Не следует, однако забывать, что консультация с широким активом требуют определения и уточнения дальнейших направлений развития. Разнообразный и богатый опыт укрепление и развитие структуры требуют определения и уточнения модели развития.',
  ' Таким образом сложившаяся структура организации представляет собой интересный эксперимент проверки существенных финансовых и административных условий. Товарищи! дальнейшее развитие различных форм деятельности играет важную роль в формировании существенных финансовых и административных условий.',
]

export const ArchiveList: React.FC = () => {
  const history = useNavigation();
  return (
    <List
      pageHeaderProps={{
        title: 'Архив',
        onBack: () => history.goBack(),
      }}
      canCreate={false}
    >
      <Row gutter={16}>
        {new Array(20).fill(1).map((_, index) => (
          <Col span={6} style={{ marginTop: 16 }}>
            <Card
              hoverable
              style={{ width: '100%' }}
              cover={<img alt="example" src={`/assets/img/(${(index * 10 % 80) + 1}).JPG`} />}
            >
              <Card.Meta title={title[index % 3]} description={text[index % 3]} />
            </Card>
          </Col>
        ))}
      </Row>
    </List>
  );
};