import {
  Create,
  Form,
  Input,
  Select,
  useForm,
  useMany,
  useSelect
} from "@pankod/refine";
import { definitions } from "interfaces/supabase";
import { DatePicker } from 'antd';

export const ActivityCreate = () => {

  const { formProps, saveButtonProps, form } = useForm<definitions['Activity']>({
    resource: 'Activity',
  });

  const { selectProps } = useSelect<definitions['Department']>({
    resource: 'Department',
    optionLabel: 'name',
    filters: [{ field: 'hidden', value: false, operator: 'eq' }],
  })

  return (
    <Create
      saveButtonProps={saveButtonProps}
      title="Добавить мероприятие"
    >
      <Form
        {...formProps}
        layout="vertical"
        onFinish={(values: any) => {
          const submitValues = {
            name: values.name,
            host_id: values.host_id,
            start_date: values.date?.[0],
            end_date: values.date?.[1],
          };
          formProps.onFinish?.(submitValues)
        }}
      >
        <Form.Item label="Название" name="name">
          <Input />
        </Form.Item>
        <Form.Item label="Даты проведения" name="date">
          <DatePicker.RangePicker
            format="DD.MM.YYYY"
            placeholder={['Дата начала', 'Дата окончания']}
          />
        </Form.Item>
        <Form.Item label="Организатор" name="host_id">
          <Select {...selectProps} />
        </Form.Item>
      </Form>
    </Create>
  );
};