import { useForm, Form, Input, Select, Edit, useSelect } from "@pankod/refine";
import { definitions } from "interfaces/supabase";
import { DatePicker, Switch } from 'antd';
import moment from 'moment';
import { useEffect } from "react";

export const ActivityEdit: React.FC = () => {
  const { formProps, saveButtonProps, queryResult, form } = useForm<definitions['Activity']>({
    resource: 'Activity',
  });

  const { selectProps } = useSelect<definitions['Department']>({
    resource: 'Department',
    optionLabel: 'name',
    filters: [{ field: 'hidden', value: false, operator: 'eq' }],
  })

  useEffect(() => {
    if (queryResult?.data?.data) {
      form.setFieldsValue({
        ...queryResult?.data?.data,
        date: [moment(queryResult?.data?.data.start_date), moment(queryResult?.data?.data.end_date)],
      });
    }
  }, [queryResult]);

  return (
    <Edit saveButtonProps={saveButtonProps} title="Редактирование мероприятия">
      <Form
        {...formProps}
        layout="vertical"
        onFinish={(values: any) => {
          const submitValues = {
            name: values.name,
            host_id: values.host_id,
            start_date: values.date?.[0],
            end_date: values.date?.[1],
          };
          formProps.onFinish?.(submitValues)
        }}
      >
        <Form.Item label="Название" name="name">
          <Input />
        </Form.Item>
        <Form.Item label="Даты проведения" name="date">
          <DatePicker.RangePicker
            format="DD.MM.YYYY"
            placeholder={['Дата начала', 'Дата окончания']}
          />
        </Form.Item>
        <Form.Item label="Организатор" name="host_id">
          <Select {...selectProps} />
        </Form.Item>
      </Form>
    </Edit>
  );
};