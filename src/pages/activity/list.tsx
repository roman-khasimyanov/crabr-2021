import {
  List,
  TextField,
  DateField,
  Table,
  useTable,
  ShowButton,
  EditButton,
  useNavigation,
  DeleteButton,
  TagField,
  useOne,
  useMany,
} from "@pankod/refine";
import { Space, Switch } from 'antd';
import { definitions } from "interfaces/supabase";

export const ActivityList: React.FC = () => {
  const { tableProps, tableQueryResult } = useTable<definitions['Activity']>({
    resource: 'Activity',
  });
  const { data: departmentData } = useMany<definitions['Department']>({
    resource: 'Department',
    ids: tableQueryResult.data?.data.map((val) => val.host_id) ?? [],
  })
  const history = useNavigation();
  console.log(history)
  return (
    <List
      pageHeaderProps={{
        title: 'Мероприятия',
        onBack: () => history.goBack(),
      }}
      createButtonProps={{
        children: 'Добавить',
        resource: 'Activity',
        onClick: (e) => {
          history.push(`activity/create`);
        }
      }}
    >
      <Table {...tableProps} rowKey="id">
        <Table.Column
          dataIndex="name"
          title="Название"
          render={(value) => <TextField value={value} />}
        />
        <Table.Column
          dataIndex="start_date"
          title="Дата начала"
          render={(value) => <DateField value={value} format="DD.MM.YYYY" />}
        />
        <Table.Column
          dataIndex="end_date"
          title="Дата окончания"
          render={(value) => <DateField value={value} format="DD.MM.YYYY" />}
        />
        <Table.Column
          dataIndex="host_id"
          title="Организатор"
          render={(value) => <TextField value={departmentData?.data.find((d) => d.id === value)?.name} />}
        />
        <Table.Column<definitions['Activity']>
          title="Действия"
          dataIndex="actions"
          render={(_text, record): React.ReactNode => {
            return (
              <Space>
                <EditButton
                  size="small"
                  recordItemId={record.id}
                  hideText
                />
                <DeleteButton
                  size="small"
                  recordItemId={record.id}
                  hideText
                />
              </Space>
            );
          }}
        />
      </Table>
    </List>
  );
};