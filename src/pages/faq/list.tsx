import {
  List,
  TextField,
  DateField,
  Table,
  useTable,
  ShowButton,
  EditButton,
  useNavigation,
  DeleteButton,
  TagField,
  useOne,
  useMany,
  Link,
  useSelect,
  useList,
} from "@pankod/refine";
import { Card, Col, Collapse, Row, Space, Switch } from 'antd';
import { definitions } from "interfaces/supabase";

const title = [
  'Ваш шедевр готов!',
  'Равным образом рамки!',
  'Место обучения кадров позволяет!',
];

const text = [
  'Товарищи! консультация с широким активом позволяет выполнять важные задания по разработке соответствующий условий активизации. Идейные соображения высшего порядка, а также постоянный количественный рост и сфера нашей активности способствует подготовки и реализации направлений прогрессивного развития. ',
  'Не следует, однако забывать, что консультация с широким активом требуют определения и уточнения дальнейших направлений развития. Разнообразный и богатый опыт укрепление и развитие структуры требуют определения и уточнения модели развития.',
  ' Таким образом сложившаяся структура организации представляет собой интересный эксперимент проверки существенных финансовых и административных условий. Товарищи! дальнейшее развитие различных форм деятельности играет важную роль в формировании существенных финансовых и административных условий.',
]

export const FAQList: React.FC = () => {
  const history = useNavigation();
  return (
    <List
      pageHeaderProps={{
        title: 'FAQ',
        onBack: () => history.goBack(),
      }}
      canCreate={false}
    >
      <Collapse defaultActiveKey={['1']}>
        {
          new Array(5).fill(1).map((_, index) => (
            <Collapse.Panel header={title[index % 3]} key={index}>
              <p>{text[index % 3]}</p>
            </Collapse.Panel>
          ))
        }
      </Collapse>
    </List>
  );
};