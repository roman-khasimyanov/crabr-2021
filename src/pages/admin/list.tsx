import { DictionariesList } from "components/dictionaries-list";
import { NextOrganizationGenerator } from "components/next-organization-generator";

export const AdminList: React.FC = () => {
  const data = [
    {
      title: 'Департаменты',
      link: 'admin/department',
    },
    {
      title: 'Мероприятия',
      link: 'admin/activity',
    },
    {
      title: 'Секции',
      link: 'admin/section',
    },
    {
      title: 'Участники',
      link: 'admin/users',
    },
    {
      title: 'События',
      link: 'admin/events',
    },
  ];

  return (
    <>
      <DictionariesList />
      <div style={{ marginTop: '15px'}}>
        <NextOrganizationGenerator />
      </div>
    </>
  )
};