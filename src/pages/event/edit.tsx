import { useForm, Form, Input, Select, Edit, useSelect, useMany, Notification, useList } from "@pankod/refine";
import { definitions } from "interfaces/supabase";
import { Button, Col, DatePicker, Divider, Row, Switch, Transfer } from 'antd';
import moment from 'moment';
import { useEffect, useState } from "react";
import { GlobalOutlined, PlusOutlined, MinusOutlined } from '@ant-design/icons';
import { supabaseClient } from "App";

export const EventEdit: React.FC = () => {


  const { formProps, saveButtonProps, queryResult, form } = useForm<definitions['Event']>({
    resource: 'Event',
  });

  const {
    formProps: speakersFormProps,
    saveButtonProps: speakerSaveButtonProps,
    queryResult: speakerQueryResult,
    form: speakerForm,
  } = useForm<definitions['EventUser']>({
    resource: 'EventUser',
    action: 'create',
  });


  const { data: speakersQueryResult, refetch } = useList<definitions['EventUser']>({
    resource: 'EventUser',
    config: {
      filters: [{ field: 'event_id', value: queryResult?.data?.data.id, operator: 'eq' }],
    },
  });

  const { data: userData } = useList<definitions['User']>({
    resource: 'User',
    config: { pagination: { pageSize: 10000 } },
  });

  useEffect(() => {
    refetch();
  }, [queryResult?.data?.data.id]);

  const [targetKeys, setTargetKeys] = useState<string[]>(speakersQueryResult?.data.map((s) => s.user_id) ?? []);
  const [selectedKeys, setSelectedKeys] = useState<string[]>([]);

  const { selectProps } = useSelect<definitions['Section']>({
    resource: 'Section',
    optionLabel: 'name',
  })

  useEffect(() => {
    if (speakersQueryResult?.data) {
      speakerForm.setFieldsValue({
        speakers: speakersQueryResult?.data.map((s) => s.user_id),
      });
      setTargetKeys(speakersQueryResult?.data.map((s) => s.user_id));
    }
  }, [speakersQueryResult]);

  useEffect(() => {
    if (queryResult?.data?.data) {
      form.setFieldsValue({
        ...queryResult?.data?.data,
        start_date: moment(queryResult?.data?.data.start_date),
        end_date: moment(queryResult?.data?.data.end_date),
      });
    }
  }, [queryResult]);

  return (
    <Edit saveButtonProps={saveButtonProps} title="Редактирование мероприятия">
      <Form
        {...formProps}
        layout="vertical"
      >
        <Form.Item label="Название" name="name">
          <Input />
        </Form.Item>
        <Form.Item label="Описание" name="description">
          <Input.TextArea />
        </Form.Item>
        <Form.Item label="Ссылка" name="link">
          <Input prefix={<GlobalOutlined />} />
        </Form.Item>
        <Form.Item label="Дата начала" name="start_date">
          <DatePicker
            format="DD.MM.YYYY HH:mm"
            showTime={{ defaultValue: moment('00:00', 'HH:mm') }}
          />
        </Form.Item>
        <Form.Item label="Дата окончания" name="end_date">
          <DatePicker
            format="DD.MM.YYYY HH:mm"
            showTime={{ defaultValue: moment('00:00', 'HH:mm') }}
          />
        </Form.Item>
        <Form.Item label="Секция" name="section_id">
          <Select {...selectProps} />
        </Form.Item>
      </Form>
      <Divider>Спикеры</Divider>
      <Transfer
        listStyle={{
          width: '100%',
          height: 300,
        }}
        dataSource={userData?.data.map((u) => ({
          key: u.id,
          title: `${u.last_name} ${u.name} ${u.second_name}`,
        }))}
        titles={['Пользователи', 'Спикеры']}
        targetKeys={targetKeys}
        selectedKeys={selectedKeys}
        onChange={async (target) => {
          const addedKeys = target.filter((t) => {
            const ind = speakersQueryResult?.data.findIndex((s) => s.user_id !== t) ?? -1;
            return ind > -1 || speakersQueryResult?.data.length === 0;
          });
          const deletedKeys = speakersQueryResult?.data.filter((s) => {
            const ind = target.findIndex((t) => s.user_id === t) ?? -1;
            return ind === -1;
          }).map((d) => d.user_id);

          await supabaseClient.from<definitions['EventUser']>('EventUser').delete().in('user_id', deletedKeys ?? []);
          await supabaseClient.from<definitions['EventUser']>('EventUser').insert(addedKeys.map((ak) => ({
            user_id: ak,
            event_id: queryResult?.data?.data.id,
          })));
          setTargetKeys(target);
          console.log(speakersQueryResult?.data, addedKeys, deletedKeys);
        }}
        onSelectChange={(sourceSelectedKeys, targetSelectedKeys) => {
          setSelectedKeys([...sourceSelectedKeys, ...targetSelectedKeys]);
        }}
        render={item => item.title}
        oneWay
        style={{ marginBottom: 16 }}
      />
    </Edit >
  );
};