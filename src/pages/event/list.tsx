import {
  List,
  TextField,
  DateField,
  Table,
  useTable,
  ShowButton,
  EditButton,
  useNavigation,
  DeleteButton,
  TagField,
  useOne,
  useMany,
  Link,
  useSelect,
  useList,
} from "@pankod/refine";
import { Space, Switch } from 'antd';
import { definitions } from "interfaces/supabase";

export const EventList: React.FC = () => {
  const { tableProps, tableQueryResult } = useTable<definitions['Event']>({
    resource: 'Event',
  });

  const { data: sectionData } = useMany<definitions['Section']>({
    resource: 'Section',
    ids: tableQueryResult.data?.data.map((val) => val.section_id ?? '') ?? [],
  });

  const { data: speakerData } = useList<definitions['EventUser']>({
    resource: 'EventUser',
    config: { pagination: { pageSize: 10000 } }
  });

  const { data: userData } = useList<definitions['User']>({
    resource: 'User',
    config: { pagination: { pageSize: 10000 } },
  });

  const { data: departments } = useList<definitions['Department']>({
    resource: 'Department',
    config: {
      filters: [{ field: 'id', value: userData?.data.map((u) => u.department_id ?? '') ?? [], operator: 'in' }],
    }
  });

  const history = useNavigation();
  console.log(history)
  return (
    <List
      pageHeaderProps={{
        title: 'События',
        onBack: () => history.goBack(),
      }}
      createButtonProps={{
        children: 'Добавить',
        resource: 'Event',
        onClick: (e) => {
          history.push(`event/create`);
        }
      }}
    >
      <Table {...tableProps} rowKey="id">
        <Table.Column
          dataIndex="name"
          title="Имя"
          render={(value) => <TextField value={value} />}
        />
        <Table.Column
          dataIndex="description"
          width="15%"
          title="Описание"
          render={(value) => <TextField value={value} />}
        />
        <Table.Column
          dataIndex="link"
          title="Ссылка"
          render={(value) => <Link to={value}>{value}</Link>}
        />
        <Table.Column
          dataIndex="start_date"
          title="Время начала"
          render={(value) => <DateField value={value} format="DD.MM.YYYY HH:mm" />}
        />
        <Table.Column
          dataIndex="end_date"
          title="Время окончания"
          render={(value) => <DateField value={value} format="DD.MM.YYYY HH:mm" />}
        />
        <Table.Column
          dataIndex="section_id"
          title="Секция"
          render={(value) => <TextField value={sectionData?.data.find((s) => s.id === value)?.name} />}
        />
        <Table.Column<definitions['Event']>
          dataIndex="speakers"
          title="Спикеры"
          render={(value, record) => {
            const filteredSpeakers = speakerData?.data
              .filter((s) => s.event_id === record.id)
              .map((s) => s.user_id);

            const filteredUsers = userData?.data.filter((u) => filteredSpeakers?.includes(u.id));
            console.log('--------------', record.id, '----------------------');
            console.log(speakerData?.data);
            console.log(filteredSpeakers);
            console.log(filteredUsers);
            return (
              <>
                {
                  filteredUsers?.map((fu) => (
                    <TagField value={`${fu.last_name} ${fu.name} ${fu.second_name} (${departments?.data.find((d) => d.id === fu.department_id)?.name ?? 'Департамент не выбран'})`} />
                  ))
                }
              </>
            )
          }}
        />
        <Table.Column<definitions['Event']>
          title="Действия"
          dataIndex="actions"
          render={(_text, record): React.ReactNode => {
            return (
              <Space>
                <EditButton
                  size="small"
                  recordItemId={record.id}
                  hideText
                />
                <DeleteButton
                  size="small"
                  recordItemId={record.id}
                  hideText
                />
              </Space>
            );
          }}
        />
      </Table>
    </List>
  );
};