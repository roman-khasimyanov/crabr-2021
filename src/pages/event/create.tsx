import {
  Create,
  Form,
  Input,
  Select,
  useForm,
  useMany,
  useSelect
} from "@pankod/refine";
import { definitions } from "interfaces/supabase";
import { DatePicker } from 'antd';
import { GlobalOutlined } from '@ant-design/icons';
import moment from "moment";

export const EventCreate = () => {

  const { formProps, saveButtonProps, form } = useForm<definitions['Event']>({
    resource: 'Event',
  });

  const { selectProps } = useSelect<definitions['Section']>({
    resource: 'Section',
    optionLabel: 'name',
  })

  return (
    <Create
      saveButtonProps={saveButtonProps}
      title="Добавить событие"
    >
      <Form
        {...formProps}
        layout="vertical"
      >
        <Form.Item label="Название" name="name">
          <Input />
        </Form.Item>
        <Form.Item label="Описание" name="description">
          <Input.TextArea />
        </Form.Item>
        <Form.Item label="Ссылка" name="link">
          <Input prefix={<GlobalOutlined />} />
        </Form.Item>
        <Form.Item label="Дата начала" name="start_date">
          <DatePicker
            format="DD.MM.YYYY HH:mm"
            showTime={{ defaultValue: moment('00:00', 'HH:mm') }}
          />
        </Form.Item>
        <Form.Item label="Дата окончания" name="end_date">
          <DatePicker
            format="DD.MM.YYYY HH:mm"
            showTime={{ defaultValue: moment('00:00', 'HH:mm') }}
          />
        </Form.Item>
        <Form.Item label="Секция" name="section_id">
          <Select {...selectProps} />
        </Form.Item>
      </Form>
    </Create>
  );
};