import {
  Create,
  Form,
  Input,
  Select,
  useForm,
  useSelect
} from "@pankod/refine";
import { definitions } from "interfaces/supabase";

export const DepartmentCreate = () => {
  const { formProps, saveButtonProps } = useForm<definitions['Department']>({
    resource: 'Department',
  });
  return (
    <Create
      saveButtonProps={saveButtonProps}
      title="Добавить департамент"
    >
      <Form {...formProps} layout="vertical">
        <Form.Item label="Название" name="name">
          <Input />
        </Form.Item>
      </Form>
    </Create>
  );
};