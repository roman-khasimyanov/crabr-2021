import { useForm, Form, Input, Select, Edit, useSelect } from "@pankod/refine";
import { definitions } from "interfaces/supabase";
import { Switch } from 'antd';
import { useEffect } from "react";

export const DepartmentEdit: React.FC = () => {
  const { formProps, saveButtonProps, queryResult, form } = useForm<definitions['Department']>({
    resource: 'Department',
  });

  useEffect(() => {
    if (queryResult?.data?.data) {
      form.setFieldsValue({
        ...queryResult?.data?.data,
      });
    }
  }, [queryResult]);

  return (
    <Edit saveButtonProps={saveButtonProps} title="Редактирование департамента">
      <Form {...formProps} layout="vertical">
        <Form.Item label="Название" name="name">
          <Input />
        </Form.Item>
        <Form.Item label="Скрыть" name="hidden" valuePropName="checked">
          <Switch />
        </Form.Item>
      </Form>
    </Edit>
  );
};