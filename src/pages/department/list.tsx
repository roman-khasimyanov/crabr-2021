import {
  List,
  TextField,
  DateField,
  Table,
  useTable,
  ShowButton,
  EditButton,
  useNavigation,
  DeleteButton,
  TagField,
} from "@pankod/refine";
import { Space, Switch } from 'antd';
import { definitions } from "interfaces/supabase";

export const DepartmentList: React.FC = () => {
  const { tableProps } = useTable<definitions['Department']>({
    resource: 'Department',
  });
  const history = useNavigation();
  console.log(history)
  return (
    <List
      pageHeaderProps={{
        title: 'Департаменты',
        onBack: () => history.goBack(),
      }}
      createButtonProps={{
        children: 'Добавить',
        resource: 'Department',
        onClick: (e) => {
          history.push(`department/create`);
        }
      }}
    >
      <Table {...tableProps} rowKey="id">
        <Table.Column
          dataIndex="name"
          title="Название"
          render={(value) => <TextField value={value} />}
        />
        <Table.Column
          dataIndex="hidden"
          title="Скрыт из списка"
          render={(value) => <TagField value={value ? 'Да' : 'Нет'} color={value ? 'red' : 'green'} />}
        />
        <Table.Column<definitions['Department']>
          title="Действия"
          dataIndex="actions"
          render={(_text, record): React.ReactNode => {
            return (
              <Space>
                <EditButton
                  size="small"
                  recordItemId={record.id}
                  hideText
                />
                <DeleteButton
                  size="small"
                  recordItemId={record.id}
                  hideText
                />
              </Space>
            );
          }}
        />
      </Table>
    </List>
  );
};