import { AntdLayout, Link, Refine, Resource } from "@pankod/refine";
import { createClient, dataProvider } from '@pankod/refine-supabase';
import { CustomSider } from "components/sider";
import "@pankod/refine/dist/styles.min.css";
import { DepartmentCreate } from "pages/department/create";
import { DepartmentEdit } from "pages/department/edit";
import { DepartmentList } from "pages/department/list";
import { AdminList } from "pages/admin/list";
import { ActivityList } from "pages/activity/list";
import { ActivityCreate } from "pages/activity/create";
import { ActivityEdit } from "pages/activity/edit";
import { SectionList } from "pages/section/list";
import { SectionCreate } from "pages/section/create";
import { SectionEdit } from "pages/section/edit";

import './App.scss';
import { Shedule } from "components/shedule";
import { UserList } from "pages/user/list";
import { UserCreate } from "pages/user/create";
import { UserEdit } from "pages/user/edit";
import { EventList } from "pages/event/list";
import { EventCreate } from "pages/event/create";
import { EventEdit } from "pages/event/edit";
import { NewsList } from "pages/news/list";
import { FAQList } from "pages/faq/list";
import { ArchiveList } from "pages/archive/list";


const supabaseClient = createClient(
  'https://ekctgodsrwugzvvfebke.supabase.co',
  'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb2xlIjoiYW5vbiIsImlhdCI6MTYzNDEwNjE4NCwiZXhwIjoxOTQ5NjgyMTg0fQ.cd8ecL2CjOCVRM-ZkaYStPYjnRProqfuca3JDiFfcMg',
);

const App: React.FC = () => {
  return (
    <Refine
      dataProvider={dataProvider(supabaseClient)}
      Layout={({ children, Footer, OffLayoutArea }) => (
        <AntdLayout>
          <AntdLayout.Header className="header">
            <div className="header__logo" />
            <div className="header__title">КРаБР</div>
            <div className="header__nav-menu">
              <CustomSider />
            </div>
          </AntdLayout.Header>
          <AntdLayout.Content>
            <AntdLayout.Content>
              <div style={{ padding: 24, minHeight: 360 }}>{children}</div>
            </AntdLayout.Content>
            <Footer />
          </AntdLayout.Content>
          <OffLayoutArea />
        </AntdLayout>
      )}
      Title={() => <></>}
    >
      <Resource
        name="news"
        options={{
          label: 'Новости',
          route: 'news'
        }}
        list={NewsList}
      />
      <Resource
        name="archive"
        options={{
          label: 'Архив',
          route: 'archive'
        }}
        list={ArchiveList}
      />
      <Resource
        name="shedule"
        options={{
          label: 'Расписание',
          route: 'shedule'
        }}
        list={Shedule}
      />
      <Resource
        name="faq"
        options={{
          label: 'FAQ',
          route: 'faq'
        }}
        list={FAQList}
      />
      <Resource
        name="admin"
        options={{
          label: 'Панель администратора',
          route: 'admin'
        }}
        list={AdminList}
      />
      <Resource
        name="Department"
        options={{
          label: 'Департаменты',
          route: 'admin/department'
        }}
        list={DepartmentList}
        create={DepartmentCreate}
        edit={DepartmentEdit}
        canDelete
      />
      <Resource
        name="Activity"
        options={{
          label: 'Департаменты',
          route: 'admin/activity'
        }}
        list={ActivityList}
        create={ActivityCreate}
        edit={ActivityEdit}
        canDelete
      />
      <Resource
        name="Section"
        options={{
          label: 'Секции',
          route: 'admin/section'
        }}
        list={SectionList}
        create={SectionCreate}
        edit={SectionEdit}
        canDelete
      />
      <Resource
        name="User"
        options={{
          label: 'Спикеры',
          route: 'admin/user'
        }}
        list={UserList}
        create={UserCreate}
        edit={UserEdit}
        canDelete
      />
      <Resource
        name="Event"
        options={{
          label: 'Спикеры',
          route: 'admin/event'
        }}
        list={EventList}
        create={EventCreate}
        edit={EventEdit}
        canDelete
      />
    </Refine>
  );
};

export { App, supabaseClient };